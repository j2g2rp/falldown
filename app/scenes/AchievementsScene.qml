/*
* Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
*
* This file is part of falldown.
*
* falldown is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; version 3.
*
* falldown is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.4
import QtQuick.Layouts 1.1

import Ubuntu.Components 1.3
import Bacon2D 1.0

import "../components"
import "../themes/colors.js" as Theme

Scene {
    property var achievementsModel

    Image {
        source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/background-tile.png")
        anchors.fill: parent
        fillMode: Image.Tile
    }

    Flickable {
        anchors.fill: parent
        contentHeight: columnLayout.height + units.gu(2)

        ColumnLayout {
            id: columnLayout

            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: units.gu(3)

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: units.gu(9)

                Image {
                    source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/top-menu-back.png")

                    fillMode: Image.TileHorizontally
                    anchors {
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }
                }

                AbstractButton {
                    height: parent.height - units.gu(2)
                    width: height

                    anchors {
                        top: parent.top
                        left: parent.left
                        leftMargin: units.gu(2)
                    }

                    onClicked: game.currentScene = mainScene

                    Image {
                        source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/home-btn.png")

                        anchors {
                            fill: parent
                            margins: units.gu(0.5)
                        }
                    }
                }
            }

            Title {
                //Layout.leftMargin: parent.width / 10
                anchors.left: parent.left
                anchors.leftMargin: parent.width / 10
                Layout.preferredWidth: parent.width / 1.25
                Layout.preferredHeight: units.gu(10)
            }

            ListView {
                id: listView
                //Layout.leftMargin: parent.width / 12
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                    right: parent.right
                    rightMargin: units.gu(2)
                }

                Layout.preferredHeight: units.gu(10) * model.count

                interactive: false

                model: achievementsModel
                delegate: Achievement {
                    height: units.gu(10)
                    width: parent.width
                }
            }
        }
    }
}
