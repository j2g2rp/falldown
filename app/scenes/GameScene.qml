/*
 * Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtMultimedia 5.4

import Ubuntu.Components 1.3
import Bacon2D 1.0

import "../components"
import "../js/game.js" as Game

Scene {
    physics: true
    clip: true

    property int score: 0
    property int lifes: 2
    property int numberOfBalls: 0
    property int oldVelocity: 0

    property bool endGame: false
    property bool smallerBall: false
    property bool slowTime: false

    property bool balloon: false
    property bool glue: false
    property bool wine: false

    property bool lostBall: false

    property bool deadAfterWine: false
    property int numberOfHolesWithoutLosingALife: 0
    property int numberOfHolesAfterWine: -1
    property int numberOfGlues: 0

    onNumberOfGluesChanged: {
        if (numberOfGlues >= 3) {
            game.unlockAchievements(['sticky']);
        }
    }

    onSlowTimeChanged: {
        if (slowTime) {
            oldVelocity = velocity;
            velocity = units.gu(0.1);
        } else {
            velocity = oldVelocity;
        }
    }

    onNumberOfBallsChanged: {
        if (numberOfBalls === 0 && gameScene.running) {
            lostBall = false;
            lostBall = true;

            if (numberOfHolesWithoutLosingALife >= 200) {
                game.unlockAchievements(['marathon', 'running', 'walking',
                    'firstSteps']);
            } else if (numberOfHolesWithoutLosingALife >= 100) {
                game.unlockAchievements(['running', 'walking', 'firstSteps']);
            } else if (numberOfHolesWithoutLosingALife >= 50) {
                game.unlockAchievements(['walking', 'firstSteps']);
            } else if (numberOfHolesWithoutLosingALife >= 10) {
                game.unlockAchievements(['firstSteps']);
            }

            numberOfHolesWithoutLosingALife = 0;

            if (lifes > 0) {
                deathSound.play();
                lifes--;
                oldVelocity = velocity;
                velocity = -units.gu(1.1);
                newLifeTimer.restart();
                Game.resetPowerupsEffects();
            }
            else {
                Game.endGame();
            }
        }
    }

    MouseArea {
        enabled: settings.control == "touch"

        onPressed: game.changeGravity(mouse.x < parent.width / 2 ? "left" : "right");
        onReleased: game.changeGravity("reset");

        anchors.fill: parent
    }

    SoundEffect {
        id: deathSound
        muted: settings.mute
        loops: 0
        source: Qt.resolvedUrl("../" + mainView.soundPrefix + "Death.wav")
    }

    onScoreChanged: {
        if (gameScene.deadAfterWine) {gameScene.deadAfterWine = false;}
        if (gameScene.numberOfHolesAfterWine !== -1) {
            gameScene.numberOfHolesAfterWine++;

            if (gameScene.numberOfHolesAfterWine >= 10) {
                game.unlockAchievements(['usedToWine']);
                game.numberOfHolesAfterWine = -1;
            }
        }
        if ((score % 10 == 0) && velocity < units.gu(3))
            velocity += 0.1;
    }

    Image {
        source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/background-tile.png")
        fillMode: Image.Tile

        anchors.fill: parent
    }

    TopMenu {
        id: menuRow
        y: gameScene.running ? -units.gu(12) : 0

        property int lifes

        Binding { target: menuRow; property: "lifes"; value: gameScene.lifes }

        Behavior on y { UbuntuNumberAnimation {} }

        anchors {
            left: parent.left
            right: parent.right
        }
    }

    /* walls */
    PhysicsEntity {
        width: units.gu(0.1)

        anchors {
            top: parent.top;
            bottom: parent.bottom;
            left: parent.left;
        }

        fixtures: [
            Edge {
                vertices: [
                    Qt.point(0, 0),
                    Qt.point(1, gameScene.height)
                ]
            }
        ]
    }

    PhysicsEntity {
        width: units.gu(0.1)

        anchors {
            top: parent.top;
            bottom: parent.bottom;
            right: parent.right;
        }

        fixtures: [
            Edge {
                vertices: [
                    Qt.point(0, 0),
                    Qt.point(-1, gameScene.height)
                ]
            }
        ]
    }

    PhysicsEntity {
        height: units.gu(0.1)
        anchors {
            top: parent.bottom;
            left: parent.left;
            right: parent.right;
        }

        fixtures: [
            Edge {
                vertices: [
                    Qt.point(0, 0),
                    Qt.point(gameScene.width, 1)
                ]
            }
        ]
    }

    PhysicsEntity {
        height: units.gu(0.1)
        anchors {
            bottom: parent.top;
            bottomMargin: units.gu(3);
            left: parent.left;
            right: parent.right;
        }

        sleepingAllowed: true

        fixtures: [
            Edge {
                vertices: [
                    Qt.point(0, 0),
                    Qt.point(gameScene.width, 1)
                ]

                sensor: true

                onBeginContact: other.getBody().target.doDestroy()
            }
        ]
    }
}
