/*
* Original work Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
* Copyright 2019 Jonatan Hatakeyama Zeidler <jonatan_zeidler@gmx.de>
*
* This file is part of falldown.
*
* falldown is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; version 3.
*
* falldown is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.4
import QtQuick.Layouts 1.1

import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import Bacon2D 1.0

import "../components"
import "../themes/colors.js" as Theme

Scene {
    Image {
        source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/background-tile.png")
        anchors.fill: parent
        fillMode: Image.Tile
    }

    Flickable {
        anchors.fill: parent
        contentHeight: columnLayout.height + units.gu(2)

        ColumnLayout {
            id: columnLayout

            anchors {
                left: parent.left
                right: parent.right
            }
            spacing: units.gu(3)

            Item {
                Layout.fillWidth: true
                Layout.preferredHeight: units.gu(9)

                Image {
                    source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/top-menu-back.png")

                    fillMode: Image.TileHorizontally
                    anchors {
                        left: parent.left
                        right: parent.right
                        bottom: parent.bottom
                    }
                }

                AbstractButton {
                    height: parent.height - units.gu(2)
                    width: height

                    anchors {
                        top: parent.top
                        left: parent.left
                        leftMargin: units.gu(2)
                    }

                    onClicked: game.currentScene = mainScene

                    Image {
                        source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/home-btn.png")

                        anchors {
                            fill: parent
                            margins: units.gu(0.5)
                        }
                    }
                }
            }

            Title {
                //Layout.leftMargin: parent.width / 10
                anchors.left: parent.left
                anchors.leftMargin: parent.width / 10
                Layout.preferredWidth: parent.width / 1.25
                Layout.preferredHeight: units.gu(10)
            }

            ColumnLayout {
                anchors.left: parent.left
                spacing: units.gu(3)

                Text {
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(3)
                    }
                    text: i18n.tr("Control")
                    color: Theme.colors[settings.theme].subtitleText
                }

                Item {
                    Layout.fillWidth: true
                    Layout.preferredHeight: units.gu(10)

                    AbstractButton {
                        width: parent.width / 2
                        height: units.gu(10)

                        anchors.left: parent.left

                        onClicked: {
                            settings.control = "tilt";
                            slider.value = settings.tiltSensivity
                        }

                        Image {
                            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/control-tilt.png")

                            height: units.gu(10)
                            width: height

                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }

                    AbstractButton {
                        width: parent.width / 2
                        height: units.gu(10)

                        onClicked: {
                            settings.control = "touch";
                            slider.value = settings.touchSensivity;
                        }

                        anchors.right: parent.right

                        Image {
                            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/control-touch.png")

                            height: units.gu(10)
                            width: height

                            anchors.centerIn: parent
                        }
                    }
                }

                Item {
                    Layout.fillWidth: true
                    Layout.preferredHeight: units.gu(7)

                    Item {
                        width: parent.width / 2
                        anchors.left: parent.left

                        Icon {
                            name: "tick"
                            height: units.gu(5)
                            width: height

                            anchors.horizontalCenter: parent.horizontalCenter

                            visible: settings.control == 'tilt'
                            color: Theme.colors[settings.theme].usualText
                        }
                    }

                    Item {
                        width: parent.width / 2
                        anchors.right: parent.right

                        Icon {
                            name: "tick"
                            height: units.gu(5)
                            width: height

                            anchors.horizontalCenter: parent.horizontalCenter

                            visible: settings.control == 'touch'
                            color: Theme.colors[settings.theme].usualText
                        }
                    }
                }

                Text {
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(3)
                    }
                    text: i18n.tr("Sensitivity")
                    color: Theme.colors[settings.theme].subtitleText
                }

                Slider {
                    id: slider
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(3)
                    }

                    //Layout.preferredWidth: parent.width - units.gu(6)

                    function formatValue(v) { return v.toFixed(1) }
                    minimumValue: 0.1
                    maximumValue: 2.0
                    value: settings.control === "tilt" ?
                                    settings.tiltSensivity :
                                    settings.touchSensivity

                    /// XXX WTF
                    onValueChanged: {
                        settings.control === "tilt" ?
                            settings.tiltSensivity = formatValue(value) :
                            settings.touchSensivity = formatValue(value)
                    }
                }

                Text {
                    anchors {
                        left: parent.left
                        leftMargin: units.gu(3)
                    }
                    text: i18n.tr("Theme")
                    color: Theme.colors[settings.theme].subtitleText
                }

                Item {
                    Layout.fillWidth: true
                    Layout.preferredHeight: units.gu(10)

                    AbstractButton {
                        width: parent.width / 2
                        height: units.gu(10)

                        anchors.left: parent.left

                        onClicked: settings.theme = 'default'

                        Image {
                            source: Qt.resolvedUrl("../themes/default/img/theme.png")

                            height: units.gu(10)
                            width: height

                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                    }

                    AbstractButton {
                        width: parent.width / 2
                        height: units.gu(10)

                        onClicked: settings.theme = 'alt'

                        anchors.right: parent.right

                        Image {
                            source: Qt.resolvedUrl("../themes/alt/img/theme.png")

                            height: units.gu(10)
                            width: height

                            anchors.centerIn: parent
                        }
                    }
                }

                Item {
                    Layout.fillWidth: true
                    Layout.preferredHeight: units.gu(7)

                    Item {
                        width: parent.width / 2
                        anchors.left: parent.left

                        Icon {
                            name: "tick"
                            height: units.gu(5)
                            width: height

                            anchors.horizontalCenter: parent.horizontalCenter

                            visible: settings.theme == 'default'
                            color: Theme.colors[settings.theme].usualText
                        }
                    }

                    Item {
                        width: parent.width / 2
                        anchors.right: parent.right

                        Icon {
                            name: "tick"
                            height: units.gu(5)
                            width: height

                            anchors.horizontalCenter: parent.horizontalCenter

                            visible: settings.theme == 'alt'
                            color: Theme.colors[settings.theme].usualText
                        }
                    }
                }
            }
        }
    }
}
