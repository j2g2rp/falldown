/*
* Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
*
* This file is part of falldown.
*
* falldown is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; version 3.
*
* falldown is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.4
import QtQuick.Layouts 1.1

import Ubuntu.Components 1.3
import Bacon2D 1.0

import "../js/game.js" as Game
import "../components"

Scene {
    Image {
        source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/background-tile.png")
        anchors.fill: parent
        fillMode: Image.Tile
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: units.gu(3)

        Item {
            Layout.fillWidth: true
            Layout.preferredHeight: units.gu(3)

            Image {
                source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/top-menu-back.png")

                fillMode: Image.TileHorizontally
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }
            }
        }

        Title {
            Layout.alignment : Qt.AlignHCenter
            Layout.preferredWidth: parent.width / 1.25
            Layout.preferredHeight: units.gu(10)
        }

        MouseArea {
            Layout.alignment : Qt.AlignHCenter
            Layout.preferredWidth: parent.width / 1.25
            Layout.preferredHeight: units.gu(16)

            Score {
                anchors.centerIn: parent
                score: settings.highScore
                stars: settings.stars
                rotating: settings.numberOfCollectedAchievements < settings.numberOfUnlockedAchievements
            }

            onClicked: game.currentScene = achievementsScene
        }

        AbstractButton {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Image {
                source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/play-btn.png")

                height: units.gu(10)
                width: height

                anchors.centerIn: parent
            }

            onClicked: Game.startGame()
        }

        Item {
            Layout.fillWidth: true
            Layout.preferredHeight: units.gu(15)

            AbstractButton {
                width: parent.width / 3
                height: units.gu(10)

                anchors.left: parent.left

                onClicked: settings.mute = !settings.mute

                Image {
                    source: settings.mute ?
                                Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/sound-on-btn-home.png") :
                                Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/sound-off-btn-home.png")

                    height: units.gu(10)
                    width: height

                    anchors.horizontalCenter: parent.horizontalCenter
                }
            }

            AbstractButton {
                width: parent.width / 3
                height: units.gu(10)

                anchors.horizontalCenter: parent.horizontalCenter

                onClicked: game.currentScene = settingsScene

                Image {
                    source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/settings-btn-home.png")

                    height: units.gu(10)
                    width: height

                    anchors.centerIn: parent
                }
            }

            AbstractButton {
                width: parent.width / 3
                height: units.gu(10)

                anchors.right: parent.right

                onClicked: game.currentScene = aboutScene

                Image {
                    source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/info-btn-home.png")

                    height: units.gu(10)
                    width: height

                    anchors.centerIn: parent
                }
            }
        }
    }
}
