/*
 * Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3

import "../themes/colors.js" as Theme

Image {
    property int score
    property int stars
    property bool rotating

    source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/score-back-1.png")
    fillMode: Image.PreserveAspectFit
    width: parent.width


    ParallelAnimation {
        loops: Animation.Infinite
        running: rotating
        SequentialAnimation {
            NumberAnimation {
                target: star_image
                property: "scale"
                duration: 333
                from: 1
                to: 2
            }
            NumberAnimation {
                target: star_image
                property: "scale"
                duration: 333
                from: 2
                to: 1
            }
            PauseAnimation {
                duration: 666
            }
        }
        RotationAnimation {
            target: star_image
            from: 0
            to: 360
            duration: 2000
        }
    }

    Text {
        text: score
        horizontalAlignment: Text.AlignHCenter

        color: Theme.colors[settings.theme].scoreText

        anchors {
            bottom: parent.bottom
            bottomMargin: parent.height * 2 / 3 - height / 2
            left: parent.left
            right: parent.right
        }
    }

    Image {
        source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/score-back-2.png")
        fillMode: Image.PreserveAspectFit

        width: parent.width
        anchors {
            bottom: parent.bottom
            left: parent.left
        }

        Image {
            id: star_image

            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/score-star.png")
            fillMode: Image.PreserveAspectFit

            height: parent.height / 3
            x: parent.height
            anchors.verticalCenter: parent.verticalCenter
        }

        Text {
            text: stars
            color: Theme.colors[settings.theme].starText
            verticalAlignment: Text.AlignVCenter

            height: parent.height / 3
            x: parent.height

            anchors {
                verticalCenter: parent.verticalCenter
                horizontalCenter: parent.horizontalCenter
            }
        }
    }

}
