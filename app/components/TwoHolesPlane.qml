/*
 * Copyright 2016 Riccardo Padovani <riccardo@rpadovani.com>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Bacon2D 1.0

import "../js/game.js" as Game

Component {
    PhysicsEntity {
        id: twoHolesPlane
        bodyType: Body.Kinematic

        sleepingAllowed: true
        linearVelocity: Qt.point(0, -velocity)
        height: units.gu(1.25)

        property bool launchedOther: false
        property int numberOfContacts: 0
        property var firstHolePosition
        property var secondHolePosition

        function doDestroy() {
            destroy();
        }

        Connections {
            target: gameScene
            onEndGameChanged: {
                if (gameScene.endGame)
                    twoHolesPlane.doDestroy();
            }
        }

        behavior: ScriptBehavior {
            script: {
                if (target.y < -units.gu(4)) {
                    twoHolesPlane.doDestroy();
                }

                if (target.launchedOther) { return; }
                if (target.y < gameScene.height - units.gu(8)){
                    target.launchedOther = true;
                    Game.addFloor(parent.width, units.gu(1));
                }
            }
        }

        fixtures: [
            Edge {
                vertices: [
                    Qt.point(0, 0),
                    Qt.point(target.firstHolePosition, 0)
                ]
            },
            Edge {
                vertices: [
                    Qt.point(target.firstHolePosition, 0),
                    Qt.point(target.firstHolePosition + units.gu(7), 0)
                ]

                sensor: true

                onBeginContact: {
                    target.numberOfContacts++;
                    if (target.numberOfContacts <= gameScene.numberOfBalls) {
                        gameScene.score++;
                        if (target.numberOfContacts === 1) {
                            gameScene.numberOfHolesWithoutLosingALife++;
                        }
                    }
                }
            },
            Edge {
                vertices: [
                    Qt.point(target.firstHolePosition + units.gu(7), 0),
                    Qt.point(target.secondHolePosition, 0)
                ]
            },
            Edge {
                vertices: [
                    Qt.point(target.secondHolePosition, 0),
                    Qt.point(target.secondHolePosition + units.gu(7), 0)
                ]

                sensor: true

                onBeginContact: {
                    target.numberOfContacts++;
                    if (target.numberOfContacts <= gameScene.numberOfBalls) {
                        gameScene.score++;
                        if (target.numberOfContacts === 1) {
                            gameScene.numberOfHolesWithoutLosingALife++;
                        }
                    }
                }
            },
            Edge {
                vertices: [
                    Qt.point(target.secondHolePosition + units.gu(7), 0),
                    Qt.point(target.width, 0)
                ]
            }
        ]

        Image {
            id: middleImage

            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/floor-middle.png")
            fillMode: Image.TileHorizontally
            width: firstHolePosition - 10

            anchors {
                left: parent.left
                top: parent.top
                bottom: parent.bottom
            }
        }

        Image {
            id: rightImage

            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/floor-right.png")
            width: 10

            anchors {
                top: parent.top
                bottom: parent.bottom
                left: middleImage.right
            }
        }

        Image {
            id: leftImage

            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/floor-left.png")
            width: 10

            anchors {
                top: parent.top
                bottom: parent.bottom
                left: rightImage.right
                leftMargin: units.gu(7)
            }
        }

        Image {
            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/floor-middle.png")
            fillMode: Image.TileHorizontally

            anchors {
                left: leftImage.right
                right: rightImageTwo.left
                top: parent.top
                bottom: parent.bottom
            }
        }

        Image {
            id: rightImageTwo

            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/floor-right.png")
            width: 10

            anchors {
                top: parent.top
                bottom: parent.bottom
                right: leftImageTwo.left
                rightMargin: units.gu(7)
            }
        }

        Image {
            id: leftImageTwo

            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/floor-left.png")
            width: 10

            anchors {
                top: parent.top
                bottom: parent.bottom
                right: middleImageTwo.left
            }
        }

        Image {
            id: middleImageTwo
            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/floor-middle.png")
            fillMode: Image.TileHorizontally
            width: parent.width - secondHolePosition - 10 - units.gu(7)

            anchors {
                right: parent.right
                top: parent.top
                bottom: parent.bottom
            }
        }
    }
}
