/*
 * Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3

import "../themes/colors.js" as Theme

Image {
    source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/title.png")

    height: units.gu(10)

    Image {
        source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/ball-back.png")

        anchors {
            left: parent.horizontalCenter
            bottom: parent.bottom
        }

        height: units.gu(5)
        width: height

        Image {
            anchors.fill: parent
            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/ball-smile.png")
        }
    }

    Text {
        anchors {
            top: parent.bottom
            left: parent.left
            leftMargin: units.gu(1.5)
        }

        text: "v " + mainView.version
        color: Theme.colors[settings.theme].versionGameText
    }
}
