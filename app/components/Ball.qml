/*
 * Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Bacon2D 1.0

Component {
    PhysicsEntity {
        id: ballEntity
        height: width
        width: gameScene.smallerBall ? units.gu(3.5) : units.gu(5)

        bodyType: Body.Dynamic

        property color ballColor: Qt.rgba(0.86, 0.28, 0.07, 1)  // #DD4814
        gravityScale: gameScene.balloon ? units.gu(0.2) : units.gu(0.5)

        fixtures: Circle {
            // This is the physic entity
            radius: target.width / 2

            density: 1
            friction: 0.8
            restitution: 0.1
        }

        Image {
            anchors.fill: parent

            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/ball-back.png")
        }

        function doDestroy() {
            gameScene.numberOfBalls--;
            destroy();
        }

        Connections {
            target: gameScene
            onEndGameChanged: {
                if (gameScene.endGame)
                    ballEntity.doDestroy();
            }
        }

        Image {
            anchors.fill: parent

            source: Qt.resolvedUrl("../" + mainView.imgPrefix + "board/ball-smile.png")
            rotation: -parent.rotation
        }
    }
}
