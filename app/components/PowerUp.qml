/*
 * Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Bacon2D 1.0

import "../js/game.js" as Game

Component {
    PhysicsEntity {
        id: powerUp
        bodyType: Body.Kinematic

        height: units.gu(6)
        width: height

        sleepingAllowed: true
        linearVelocity: Qt.point(0, -velocity)

        property var typeOfPowerUp

        function doDestroy() {
            destroy();
        }

        Connections {
            target: gameScene
            onEndGameChanged: {
                if (gameScene.endGame)
                    powerUp.doDestroy();
            }
        }

        fixtures: [
            Box {
                sensor: true
                width: target.width
                height: target.height

                onBeginContact: {
                    var body = other.getBody();
                    if (powerUp.typeOfPowerUp === 'threeBalls') {
                        multiplySound.play();
                        gameScene.numberOfGlues = 0;
                        game.usePowerup(powerUp.typeOfPowerUp)
                        Game.addBall(body.target.x, body.target.y - body.target.height / 2);
                        Game.addBall(body.target.x, body.target.y + body.target.height / 2);
                    } else if (powerUp.typeOfPowerUp === 'smallBall') {
                        shrinkSound.play();
                        gameScene.numberOfGlues = 0;
                        gameScene.smallerBall = true;
                        game.usePowerup(powerUp.typeOfPowerUp)
                        smallerBallTimer.restart();
                    } else if (typeOfPowerUp === 'slowTime') {
                        clockSound.play();
                        gameScene.numberOfGlues = 0;
                        gameScene.slowTime = true;
                        game.usePowerup(powerUp.typeOfPowerUp)
                        slowTimeTimer.restart();
                    } else if (powerUp.typeOfPowerUp === 'heart') {
                        heartSound.play();
                        gameScene.numberOfGlues = 0;
                        game.usePowerup(powerUp.typeOfPowerUp)
                        gameScene.lifes = gameScene.lifes + 1;
                    } else if (powerUp.typeOfPowerUp === 'balloon') {
                        balloonSound.play();
                        gameScene.numberOfGlues = 0;
                        gameScene.balloon = true;
                        balloonTimer.restart();
                    } else if (powerUp.typeOfPowerUp === 'glue') {
                        glueSound.play();
                        gameScene.glue = true;
                        gameScene.numberOfGlues++;
                        glueTimer.restart();
                    } else if (powerUp.typeOfPowerUp === 'wine') {
                        wineSound.play();
                        gameScene.wine = true;
                        gameScene.numberOfGlues = 0;
                        gameScene.deadAfterWine = true;
                        wineTimer.restart();
                    }

                    target.doDestroy();
                }
            }
        ]

        Image {
            source: {
                if (powerUp.typeOfPowerUp === 'threeBalls')
                    return Qt.resolvedUrl("../" + mainView.imgPrefix + "board/elixir-3-balls.png");
                if (powerUp.typeOfPowerUp === 'smallBall')
                    return Qt.resolvedUrl("../" + mainView.imgPrefix + "board/elixir-get-smaller.png");
                if (powerUp.typeOfPowerUp === 'slowTime')
                    return Qt.resolvedUrl("../" + mainView.imgPrefix + "board/clock-slow-lvl.png");
                if (powerUp.typeOfPowerUp === 'heart')
                    return Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/heart.png");
                if (powerUp.typeOfPowerUp === 'balloon')
                    return Qt.resolvedUrl("../" + mainView.imgPrefix + "board/balloon.png");
                if (powerUp.typeOfPowerUp === 'glue')
                    return Qt.resolvedUrl("../" + mainView.imgPrefix + "board/glue.png");
                if (powerUp.typeOfPowerUp === 'wine')
                    return Qt.resolvedUrl("../" + mainView.imgPrefix + "board/wine.png");

                // The component has been created but still not instantiated
                return Qt.resolvedUrl("../themes/null.png");
            }
            anchors.fill: parent
        }

        behavior: ScriptBehavior {
            script: {
                if (target.y < -units.gu(4)) {
                    powerUp.doDestroy();
                }
            }
        }
    }
}
