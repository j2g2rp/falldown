/*
 * Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import Bacon2D 1.0

import "../js/game.js" as Game

Item {
    height: units.gu(20)
    z: 100

    Image {
        source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/top-menu-back.png")
        fillMode: Image.TileHorizontally

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
    }

    Item {
        id: topRow
        height: units.gu(12)
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }

        AbstractButton {
            height: parent.height - units.gu(2)
            width: height

            anchors.top: parent.top

            onClicked: settings.mute = !settings.mute

            Image {
                source: settings.mute ?
                            Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/sound-on-btn.png") :
                            Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/sound-off-btn.png")

                anchors {
                    fill: parent
                    margins: units.gu(0.5)
                }
            }
        }

        AbstractButton {
            height: parent.height - units.gu(2)
            width: height

            anchors {
                top: parent.top
                horizontalCenter: parent.horizontalCenter
            }

            onClicked: Game.endGame()

            Image {
                source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/home-btn.png")

                anchors {
                    fill: parent
                    margins: units.gu(0.5)
                }
            }
        }

        AbstractButton {
            height: parent.height - units.gu(2)
            width: height

            anchors {
                top: parent.top
                right: parent.right
            }

            onClicked: Game.restartGame();

            Image {
                source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/reset-btn.png")

                anchors {
                    fill: parent
                    margins: units.gu(0.5)
                }
            }
        }
    }

    Item {
        id: bottomRow
        height: units.gu(8)
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        AbstractButton {
            id: pauseBtn

            height: parent.height - units.gu(2)
            width: height

            anchors.top: parent.top

            onClicked: {
                if (game.gameState == Bacon2D.Paused) {
                    game.gameState = Bacon2D.Running;
                    pauseImg.source = Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/pause-btn.png");
                } else {
                    game.gameState = Bacon2D.Paused;
                    pauseImg.source = Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/play-btn.png");
                }
            }

            Image {
                id: pauseImg
                source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/pause-btn.png")

                Connections {
                    target: gameScene
                    onEndGameChanged: {
                        if (gameScene.endGame)
                            pauseImg.source = Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/pause-btn.png");
                    }
                }

                // XXX force the update, trolololol
                Connections {
                    target: settings
                    onThemeChanged: {
                        pauseImg.source = Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/pause-btn.png");
                    }
                }

                anchors {
                    fill: parent
                    margins: units.gu(1)
                }
            }
        }

        Label {
            text: score

            anchors {
                verticalCenter: pauseBtn.verticalCenter
                horizontalCenter: bottomRow.horizontalCenter
            }

            horizontalAlignment: Text.AlignHCenter
        }

        Row {
            id: lifesRow
            anchors {
                top: parent.top
                right: parent.right
                rightMargin: units.gu(1)
            }

            height: parent.height - units.gu(2)

            Repeater {
                model: lifes

                delegate: Image {
                    source: Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/heart.png")
                    width: height

                    anchors {
                        top: lifesRow.top
                        bottom: lifesRow.bottom
                        margins: units.gu(1)
                    }
                }
            }
        }
    }
}
