/*
 * Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.4

import "../themes/colors.js" as Theme

Item {
    id: achievement

    ParallelAnimation {
        loops: Animation.Infinite
        running: model.unlocked && !model.collected
        SequentialAnimation {
            NumberAnimation {
                target: image
                property: "scale"
                duration: 333
                from: 1
                to: 2
            }
            NumberAnimation {
                target: image
                property: "scale"
                duration: 333
                from: 2
                to: 1
            }
            PauseAnimation {
                duration: 666
            }
        }
        RotationAnimation {
            target: image
            from: 0
            to: 360
            duration: 2000
        }
    }

    Item {
        anchors {
            left: parent.left
            rightMargin: units.gu(1)
            right: image.left
            top: parent.top
            bottom: parent.bottom
        }

        Text {
            id: name
            text: model.name
            wrapMode: Text.Wrap
            font.bold: true

            anchors {
                left: parent.left
                right: parent.right
            }

            color: model.collected ? Theme.colors[settings.theme].achievementTitleCollected :
                    model.unlocked ? Theme.colors[settings.theme].achievementTitleUnlocked :
                        Theme.colors[settings.theme].achievementTitleLocked
        }

        Text {
            anchors.top: name.bottom
            text: model.description
            wrapMode: Text.Wrap

            anchors {
                left: parent.left
                right: parent.right
            }

            color: Theme.colors[settings.theme].achievementDescription
        }
    }

    Text {
        id: numberOfStars
        visible: !model.collected && model.unlocked
        width: units.gu(2.5)
        height: width
        anchors.right: image.left

        text: '+' + model.starsAward
        color: Theme.colors[settings.theme].achievementTitleUnlocked
    }

    Image {
        id: image
        height: !model.collected && model.unlocked ? units.gu(2.5) : units.gu(5)
        width: height
        anchors.right: parent.right

        source: model.collected ?
                    Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/tick.png") :
                    model.unlocked ?
                        Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/score-star.png") :
                        Qt.resolvedUrl("../" + mainView.imgPrefix + "ui/lock.png")
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (model.unlocked) {
                game.collectAchievement(model.key)
            }
        }
    }
}
