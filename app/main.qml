/*
 * Original work Copyright 2015-2016 Riccardo Padovani <riccardo@rpadovani.com>
 * Copyright 2019 Jonatan Hatakeyama Zeidler <jonatan_zeidler@gmx.de>
 *
 * This file is part of falldown.
 *
 * falldown is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * falldown is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import QtQml 2.2
import QtQuick 2.4
import QtSensors 5.0
import QtSystemInfo 5.0
import QtMultimedia 5.4
import Ubuntu.Components 1.3
import Bacon2D 1.0

import "components"
import "scenes"
import "../js/game.js" as Game

MainView {
    id: mainView
    applicationName: "falldown.jonnius"

    height: units.gu(68)
    width: units.gu(44)

    readonly property var version: "0.5.0"
    property real velocity: units.gu(0.3)

    property var soundPrefix: "themes/default/sounds/" //+ settings.theme + "/sounds/"
    property var imgPrefix: "themes/" + settings.theme + "/img/"

    Game {
        id: game
        anchors.centerIn: parent
        anchors.fill: parent

        currentScene: mainScene

        property var powerupsUsed: []

        function usePowerup(name) {
            if (powerupsUsed.indexOf(name) === -1) {
                powerupsUsed.push(name);
            }

            if (powerupsUsed.length === 4) {
                game.unlockAchievements(['tryThemAll']);
            }
        }
        property var achievements
        property var basicAchievements: {
            "firstSteps": {
                "name": i18n.tr("First steps"),
                "description": i18n.tr("Pass 10 floors without losing a life"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "firstSteps"
            },
            "walking": {
                "name": i18n.tr("I'm walking"),
                "description": i18n.tr("Pass 50 floors without losing a life"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "walking"
            },
            "running": {
                "name": i18n.tr("I'm running!"),
                "description": i18n.tr("Pass 100 floors without losing a life"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "running"
            },
            "marathon": {
                "name": i18n.tr("Marathon"),
                "description": i18n.tr("Pass 200 floors without losing a life"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "marathon"
            },
            "multiply": {
                "name": i18n.tr("Multiply"),
                "description": i18n.tr("Have 6 balls on the screen at once"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "multiply"
            },
            "clonesBrothers": {
                "name": i18n.tr("Clones or brothers?"),
                "description": i18n.tr("Have 10 balls on the screen at once"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "clonesBrothers"
            },
            "drinking": {
                "name": i18n.tr("It's hard after drinking"),
                "description": i18n.tr("Doesn't pass any floor after getting wine glass"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "drinking"
            },
            "usedToWine": {
                "name": i18n.tr("Getting used to wine"),
                "description": i18n.tr("Pass 10 floors after getting wine glass"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "usedToWine"
            },
            "sticky": {
                "name": i18n.tr("Sticky fingers"),
                "description": i18n.tr("Get 3 glues one after another"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "sticky"
            },
            "tryThemAll": {
                "name": i18n.tr("Try them all"),
                "description": i18n.tr("Get every possible powerup without losing life"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "tryThemAll"
            },
            "start": {
                "name": i18n.tr("It's a start"),
                "description": i18n.tr("Play 10 games"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "start"
            },
            "addicted": {
                "name": i18n.tr("Are you addicted?"),
                "description": i18n.tr("Play 50 games"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "addicted"
            },
            "stop": {
                "name": i18n.tr("Maybe it's time to stop"),
                "description": i18n.tr("Play 100 games"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "stop"
            },
            // "supporter": {
            //     "name": i18n.tr("Supporter"),
            //     "description": i18n.tr("Donate to Falldown team with In App Button"),
            //     "unlocked": false,
            //     "collected": false,
            //     "starsAward": 1,
            //     "key": "supporter"
            // },
            "done": {
                "name": i18n.tr("And we're done"),
                "description": i18n.tr("Take all achievements"),
                "unlocked": false,
                "collected": false,
                "starsAward": 1,
                "key": "done"
            }
        }
        // XXX: remember to change number of achievements needed to unlock the
        // done achievement in onNumberOfUnlockedAchievementsChanged

        ListModel { id: achievementsListModel }

        function unlockAchievements(names) {
            for (var i in names) {
                var name = names[i];
                if (game.achievements[name] && !game.achievements[name].unlocked) {
                    game.achievements[name].unlocked = true;
                    settings.numberOfUnlockedAchievements++;
                }
            }

            settings.achievements = JSON.stringify(game.achievements)
            reloadAchievements();
        }

        function collectAchievement(name) {
            if (game.achievements[name] && game.achievements[name].unlocked && !game.achievements[name].collected) {
                game.achievements[name].collected = true;
                settings.numberOfCollectedAchievements++;
                settings.stars += game.achievements[name].starsAward;
                reloadAchievements();
            }

            settings.achievements = JSON.stringify(game.achievements)
        }

        function reloadAchievements() {
            achievementsListModel.clear()
            var orderedList = ["firstSteps", "walking", "running", "marathon",
                "multiply", "clonesBrothers", "drinking", "usedToWine",
                "sticky", "tryThemAll", "start", "addicted", "stop",
                "done"];

            for (var a in orderedList) {
                if (game.achievements[orderedList[a]].unlocked) {
                    achievementsListModel.append(game.achievements[orderedList[a]]);
                }
            }

            for (var a in orderedList) {
                if (!game.achievements[orderedList[a]].unlocked) {
                    achievementsListModel.append(game.achievements[orderedList[a]]);
                }
            }

            achievementsScene.achievementsModel = achievementsListModel
        }

        ScreenSaver { screenSaverEnabled: !gameScene.running }

        Component.onCompleted: {
            if (!settings.achievements || settings.achievements === 'firstStart') {
                game.achievements = game.basicAchievements
                settings.achievements = JSON.stringify(game.basicAchievements)
            } else {
                game.achievements = JSON.parse(settings.achievements)
                for (var a in game.basicAchievements) {
                    if (!game.achievements[a]) {
                        game.achievements[a] = game.basicAchievements[a]
                    }
                }
                settings.achievements = JSON.stringify(game.achievements)
            }
            reloadAchievements();
        }

        Settings {
            id: settings
            property int highScore: 0
            property bool mute: false
            property var control: "tilt"
            property real tiltSensivity: 1.0
            property real touchSensivity: 1.0
            property var theme: "default"
            property int stars: 0
            property string achievements: "firstStart"
            property int numberOfPlayedGames: 0
            property int numberOfUnlockedAchievements: 0
            property int numberOfCollectedAchievements: 0

            onMuteChanged: mute ? soundtrack.stop() : soundtrack.play()
            onNumberOfUnlockedAchievementsChanged: {
                // XXX update this number if you add an achievement.
                // It has to be achievements.length - 1
                if (numberOfUnlockedAchievements === 14) {
                    game.unlockAchievements(['done']);
                }
            }
        }

        Connections {
            target: Qt.application
            onActiveChanged: {
                if (Qt.application.active && !settings.mute) {
                    soundtrack.play();
                } else {
                    soundtrack.pause();
                }
            }
        }

        MainScene {
            id: mainScene
            anchors.fill: parent
        }

        GameScene {
            id: gameScene
            anchors.fill: parent

            gravity: Qt.point(0, 20)
        }

        AboutScene {
            id: aboutScene
            anchors.fill: parent
        }

        EndScene {
            id: endScene
            anchors.fill: parent
        }

        SettingsScene {
            id: settingsScene
            anchors.fill: parent
        }

        AchievementsScene {
            id: achievementsScene
            anchors.fill: parent
        }

        Plane { id: planeStatic }
        TwoHolesPlane { id: twoHolesPlaneStatic }
        Ball { id: ballStatic }
        PowerUp { id: powerUpStatic }

        Timer {
            id: smallerBallTimer
            interval: 5000

            onTriggered: gameScene.smallerBall = false;
        }

        Timer {
            id: slowTimeTimer
            interval: 2125

            onTriggered: gameScene.slowTime = false;
        }

        Timer {
            id: balloonTimer
            interval: 3000

            onTriggered: gameScene.balloon = false;
        }

        Timer {
            id: glueTimer
            interval: 8000

            onTriggered: gameScene.glue = false;
        }

        Timer {
            id: wineTimer
            interval: 5000

            onTriggered: gameScene.wine = false;
        }

        Timer {
            id: newLifeTimer
            interval: 1500

            onTriggered: {
                velocity = gameScene.oldVelocity;
                Game.addBall();
            }
        }

        SoundEffect {
            id: balloonSound
            muted: settings.mute
            loops: 0
            source: Qt.resolvedUrl(mainView.soundPrefix + "Balloon.wav")
        }

        SoundEffect {
            id: clockSound
            muted: settings.mute
            loops: 0
            source: Qt.resolvedUrl(mainView.soundPrefix + "Clock.wav")
        }

        SoundEffect {
            id: glueSound
            muted: settings.mute
            loops: 0
            source: Qt.resolvedUrl(mainView.soundPrefix + "Glue.wav")
        }

        SoundEffect {
            id: heartSound
            muted: settings.mute
            loops: 0
            source: Qt.resolvedUrl(mainView.soundPrefix + "Heart.wav")
        }

        SoundEffect {
            id: multiplySound
            muted: settings.mute
            loops: 0
            source: Qt.resolvedUrl(mainView.soundPrefix + "Multiply.wav")
        }

        SoundEffect {
            id: shrinkSound
            muted: settings.mute
            loops: 0
            source: Qt.resolvedUrl(mainView.soundPrefix + "Shrink.wav")
        }

        SoundEffect {
            id: swollowSound
            muted: settings.mute
            loops: 0
            source: Qt.resolvedUrl(mainView.soundPrefix + "Swollow.wav")
        }

        SoundEffect {
            id: wineSound
            muted: settings.mute
            loops: 0
            source: Qt.resolvedUrl(mainView.soundPrefix + "Wine.wav")
        }

        Audio {
            id: soundtrack
            source: "file://" + soundsDir + "/soundtrack.mp3"
            muted: settings.mute
            loops: Audio.Infinite
            //autoPlay: true
            volume: 0.7
        }

        function changeGravity(direction) {
            var inverted = gameScene.wine ? -1 : 1;
            var ballVelocity = gameScene.glue ? 3 : 20;
            ballVelocity = ballVelocity * inverted * settings.touchSensivity;

            if (direction === "left") {
                gameScene.gravity = Qt.point(-ballVelocity, 20);
            } else if (direction === "right") {
                gameScene.gravity = Qt.point(ballVelocity, 20);
            } else if (direction === "reset") {
                gameScene.gravity = Qt.point(0, 20);
            } else {
                console.log("Wrong param for changeGravity()");
            }
        }

        Keys.onPressed: {
            if (event.key === Qt.Key_Left) {
                changeGravity("left");
            }

            if (event.key === Qt.Key_Right) {
                changeGravity("right");
            }
        }

        Keys.onReleased: changeGravity("reset");

        Accelerometer {
            active: settings.control == 'tilt'

            onReadingChanged: {
                var inverted = gameScene.wine ? -1 : 1;
                var ballVelocity = gameScene.glue ? -2 : -12;

                gameScene.gravity = Qt.point(settings.tiltSensivity * ballVelocity * reading.x * inverted, 20)
            }
        }
    }
}
